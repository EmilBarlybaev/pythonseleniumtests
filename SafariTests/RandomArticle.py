from selenium import webdriver
from selenium.webdriver.common.by import By

#Got to Random article in Wiki

driver = webdriver.Safari()

driver.get("https://ru.wikipedia.org/wiki/")

linkRandom = driver.find_element(by=By.LINK_TEXT, value="Случайная статья")
linkRandom.click()

titleArticle = driver.find_element(by=By.ID, value="firstHeading").text
linkArticle = driver.current_url
print(titleArticle + "\n" +linkArticle)

driver.quit()

