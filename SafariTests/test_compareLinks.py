#   Результат поиска. Кнопка "Мне повезёт" ведёт на первую ссылку из результатов "Поиск Google"
#   Сценарий:
#       1. Откройте браузер
#       2. Перейдите на страницу https://www.google.com/
#       3. В поле для поиска введите значения "Википедия"
#       4. Нажмите на кнопку "Поиск в Google"
#       5. Зафиксируйте ссылку первого результата из списка
#       6. Откройте новую вкладку
#       7. Перейдите на страницу https://www.google.com/
#       8. В поле для поиска введите значения "Википедия"
#       9. Нажмите на кнопку "Мне повезёт"
#       10. Зафиксируйте ссылку
#   Ожидаемый результат: Первая ссылка из результатов по кнопке "Поиск в Google" и результат по кнопке "Мне повезёт" совпадают


from selenium import webdriver
from selenium.webdriver.common.by import By

def test_compareLinks():

    driver = webdriver.Safari()

    driver.get("https://www.google.com/")

    inputSearch = driver.find_element(By.CSS_SELECTOR, 'form input[name="q"]')
    inputSearch.send_keys("Википедия")

    buttonSearch = driver.find_element(By.CSS_SELECTOR, 'center:nth-child(1) [name="btnK"]')
    buttonSearch.click()

    searchLink = driver.find_element(By.CSS_SELECTOR, "div.g div.g > div > div >div > a").get_attribute("href")

    driver.switch_to.new_window("tab")

    driver.get("https://www.google.com/")

    inputSearch = driver.find_element(By.CSS_SELECTOR, 'form input[name="q"]')
    inputSearch.send_keys("Википедия")

    buttonLucky = driver.find_element(By.CSS_SELECTOR, 'center:nth-child(1) [name="btnI"]')
    buttonLucky.click()

    luckyLink = driver.current_url

    driver.quit()

    assert searchLink == luckyLink
